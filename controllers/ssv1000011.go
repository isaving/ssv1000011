//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000011/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000011Controller struct {
	controllers.CommController
}

func (*Ssv1000011Controller) ControllerName() string {
	return "Ssv1000011Controller"
}

// @Desc ssv1000011 controller
// @Author
// @Date 2020-12-05
func (c *Ssv1000011Controller) Ssv1000011() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000011Controller.Ssv1000011 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000011I := &models.SSV1000011I{}
	if err := models.UnPackRequest(c.Req.Body, ssv1000011I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv1000011I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000011 := &services.Ssv1000011Impl{}
	ssv1000011.New(c.CommController)
	ssv1000011.Sv100011I = ssv1000011I

	ssv1000011O, err := ssv1000011.Ssv1000011(ssv1000011I)

	if err != nil {
		log.Errorf("Ssv1000011Controller.Ssv1000011 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv1000011O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}

// @Title Ssv1000011 Controller
// @Description ssv1000011 controller
// @Param Ssv1000011 body model.SSV1000011I true body for SSV1000011 content
// @Success 200 {object} model.SSV1000011O
// @router /create [post]
func (c *Ssv1000011Controller) SWSsv1000011() {
	//Here is to generate API documentation, no need to implement methods
}
