//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000011/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
)

type Ssv1000011Impl struct {
	services.CommonService
	//TODO ADD Service Self Define Field
	Sv100011O *models.SSV1000011O
	Sv100011I *models.SSV1000011I
	Ac000002O *models.SAC0000002O
}

// @Desc Ssv1000011 process
// @Author
// @Date 2020-12-05
func (impl *Ssv1000011Impl) Ssv1000011(ssv1000011I *models.SSV1000011I) (ssv1000011O *models.SSV1000011O, err error) {

	impl.Sv100011I = ssv1000011I

	//调用核算账户信息查询bss
	if err := impl.QueryAccountInfo(); err != nil {
		return nil, err
	}

	if len(impl.Ac000002O.AResult) == 0 {
		return nil, errors.New("record not found", constant.ERRCODE01)
	}

	//初始化切片
	Ssv10011Records := []models.Ssv10011Record{}

	//组建返回报文
	if len(impl.Ac000002O.AResult) > 0 {
		for _, data := range impl.Ac000002O.AResult {
			sv10011Record := models.Ssv10011Record{
				Account:        data.Account,        //合约号
				AccountingId:   data.AccountingId,   //核算账号
				AmtFrozen:      data.AmtFrozen,      //冻结金额
				AmtCurrent:     data.AmtCurrent,     //当前余额
				AmtLast:        data.AmtLast,        //上期余额
				OpenDate:       data.OpenDate,       //开户日期
				LastActiveDate: data.LastActiveDate, //最后活动日期
				AccStatus:      data.AccStatus,      //账户状态
				AvalAmount:     data.AvalAmount,     //可用余额
			}
			Ssv10011Records = append(Ssv10011Records, sv10011Record)
		}
	}

	ssv1000011O = &models.SSV1000011O{
		AResult: Ssv10011Records,
	}

	return ssv1000011O, nil
}

//核算账户查询
func (impl *Ssv1000011Impl) QueryAccountInfo() error {
	ac000002I := models.SAC0000002I{
		AList: impl.Sv100011I.AList,
	}

	//pack request
	reqBody, err := ac000002I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.AC000002, reqBody)
	if err != nil {
		return err
	}

	ac000002O := models.SAC0000002O{}
	err = ac000002O.UnPackResponse(resBody)
	if err != nil {
		return err
	}

	impl.Ac000002O = &ac000002O

	return nil
}
