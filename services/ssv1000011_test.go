//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"testing"
)

var AC000002 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
        "AResult": [
            {
                "Account": "1",
                "AccountingId": "1",
                "AmtFrozen": 100,
                "AmtCurrent": 100,
                "AmtLast": 100,
                "OpenDate": "1",
                "LastActiveDate": "1",
                "AccStatus":"1",
                "AvalAmount":100
            }
        ]
    }
}`

func (this *Ssv1000011Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	switch serviceKey {
	case "AC000002":
		responseData = []byte(AC000002)

	}

	return responseData, nil
}

func TestSsv1000011Impl_Ssv1000011(t *testing.T) {

	Ssv1000011Impl := new(Ssv1000011Impl)

	//传空参
	_, _ = Ssv1000011Impl.Ssv1000011(&models.SSV1000011I{})

	_, _ = Ssv1000011Impl.Ssv1000011(&models.SSV1000011I{
		AList: []string{"1", "2"},
	})

}
