#!/bin/bash
set -e -x

cd $(dirname $0)/..
go test -coverprofile=coverage.out -count=1 ./services/.
