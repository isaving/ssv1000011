package model

type SSV1000011I struct {
	AList []string `json:"AList" description:"Accounting account number array"` //核算账号数组
}

type SSV1000011O struct {
	AResult []Ssv10011Record `json:"AResult" description:"Accounting account information"`
}

type Ssv10011Record struct {
	Account        string `json:"Account" description:"Contract number"`        //合约号
	AccountingId   string `json:"AccountingId" description:"Accounting account number"`   //核算账号
	AmtFrozen      string `json:"AmtFrozen" description:"Frozen amount"`      //冻结金额
	AmtCurrent     string `json:"AmtCurrent" description:"Current balance"`     //当前余额
	AmtLast        string `json:"AmtLast" description:"Previous balance"`        //上期余额
	OpenDate       string `json:"OpenDate" description:"Account opening date"`       //开户日期
	LastActiveDate string `json:"LastActiveDate" description:"Last activity date"` //最后活动日期
	AccStatus      string `json:"AccStatus" description:"Account status"`      //账户状态
}
