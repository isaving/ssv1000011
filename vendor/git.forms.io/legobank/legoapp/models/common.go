package models

type CommonResponse struct {
	ReturnCode string      `json:"returnCode"`
	ReturnMsg  string      `json:"returnMsg"`
	Data       interface{} `json:"data"`
}

const (
	successCode = "0"
	successMsg  = "success"
)
