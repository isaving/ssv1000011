//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package log

// func init() {
// 	fmt.Println("load log config")
// 	defer seelog.Flush()
// 	//加载配置文件
// 	logger, err := seelog.LoggerFromConfigAsFile("log.xml")
// 	if err != nil {
// 		panic("parse config.xml error")
// 	}
// 	//替换记录器
// 	seelog.ReplaceLogger(logger)
// }
import (
	"fmt"
	"github.com/go-errors/errors"
	"net"
	"net/http"
	"os"

	"github.com/astaxie/beego"
	"github.com/sirupsen/logrus"
	"github.com/snowzach/rotatefilehook"
)

const (
	timeFormat = "2006-01-02 15:04:05"
)

//define log level
var logLevelMap = map[string]logrus.Level{
	"debug":   logrus.DebugLevel,
	"info":    logrus.InfoLevel,
	"warning": logrus.WarnLevel,
	"error":   logrus.ErrorLevel,
	"fatal":   logrus.FatalLevel,
	"panic":   logrus.PanicLevel,
}

//set log level
func setLogLevel() {
	logLevelConf := beego.AppConfig.String("log::logLevel")
	logLevel, ok := logLevelMap[logLevelConf]
	if ok {
		logrus.SetLevel(logLevel)
	}
}

//set log rotate hook
func setLogRotateHook(logFile string) {
	maxSize, err := beego.AppConfig.Int("log.maxSize")
	if err != nil {
		maxSize = 200
	}

	maxAge, err := beego.AppConfig.Int("log.maxDays")
	if err != nil {
		maxAge = 7
	}

	logLevelConf := beego.AppConfig.String("log::logLevel")
	logLevel, ok := logLevelMap[logLevelConf]
	if !ok {
		logLevel = logrus.DebugLevel
	}

	customFormatter := logrus.JSONFormatter{}
	customFormatter.TimestampFormat = timeFormat

	rotateFileHook, err := rotatefilehook.NewRotateFileHook(rotatefilehook.RotateFileConfig{
		Filename:   logFile,
		MaxSize:    maxSize,
		MaxBackups: maxAge,
		MaxAge:     maxAge,
		Level:      logLevel,
		Formatter:  &customFormatter,
	})

	logrus.AddHook(rotateFileHook)
}

//init log
func InitLog() error {
	logFile := beego.AppConfig.String("log::logFile")
	if logFile != "" {
		f, err := os.OpenFile(logFile, os.O_WRONLY|os.O_CREATE, 0755)
		if err != nil {
			logrus.Errorf("Failed to open log file %s", logFile)
			return errors.Wrap(err, 0)
		}
		logrus.SetOutput(f)
	}
	// 添加将日志打印到console的处理
	logrus.SetOutput(os.Stdout)
	setLogLevel()
	startLogLevelServer()
	setLogRotateHook(logFile)
	//	logrus.SetReportCaller(true)

	return nil
}

func getLogFields() logrus.Fields {
	fields := logrus.Fields{}
	/*if SubsystemName != "" {
		fields["subsystemName"] = SubsystemName
	}

	keys := []string{
		GLS_TRANSACTION_ID,
		GLS_USER_NAME,
		GLS_TRANSACTION_DESCRIPTION,
	}

	for _, key := range keys {
		if value := gls.Get(key); value != nil {
			fields[key] = value
		}
	}*/
	return fields
}

func Infof(format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Infof(format, args...)
	}
}

func Info(args ...interface{}) {
	if logrus.GetLevel() >= logrus.InfoLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Info(args...)
	}
}

func Errorf(format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Errorf(format, args...)
	}
}

func Error(args ...interface{}) {
	if logrus.GetLevel() >= logrus.ErrorLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Error(args...)
	}
}

func Debugf(format string, args ...interface{}) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Debugf(format, args...)
	}
}

func Debug(args ...interface{}) {
	if logrus.GetLevel() >= logrus.DebugLevel {
		fields := getLogFields()
		logrus.WithFields(fields).Debug(args...)
	}
}

//---------------------------下面的代码用于动态设定日志等级--------------------------------//

// Server structure is used to the store backend information
type Server struct {
	SocketLocation string
	Debug          bool
}

// StartServerWithDefaults starts the server with default values
func startLogLevelServer() {
	s := Server{
		SocketLocation: beego.AppConfig.String("log::logLevelUnixSocket"),
	}
	s.Start()
}

// Start the server
func (s *Server) Start() {
	os.Remove(s.SocketLocation)
	go s.ListenAndServe()
}

// ListenAndServe is used to setup handlers and
// start listening on the specified location
func (s *Server) ListenAndServe() error {
	logrus.Infof("Listening on %s", s.SocketLocation)
	server := http.Server{}
	http.HandleFunc("/v1/loglevel", s.loglevel)
	socketListener, err := net.Listen("unix", s.SocketLocation)
	if err != nil {
		return err
	}
	return server.Serve(socketListener)
}

func (s *Server) loglevel(rw http.ResponseWriter, req *http.Request) {
	// curl -X POST -d "level=debug" localhost:12345/v1/loglevel
	logrus.Debugf("Received loglevel request")
	if req.Method == http.MethodGet {
		level := logrus.GetLevel().String()
		rw.Write([]byte(fmt.Sprintf("%s\n", level)))
	}

	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			rw.Write([]byte(fmt.Sprintf("Failed to parse form: %v\n", err)))
		}
		level, err := logrus.ParseLevel(req.Form.Get("level"))
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			rw.Write([]byte(fmt.Sprintf("Failed to parse loglevel: %v\n", err)))
		} else {
			logrus.SetLevel(level)
			rw.Write([]byte("OK\n"))
		}
	}
}
