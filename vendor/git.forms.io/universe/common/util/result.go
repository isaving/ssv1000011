//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package util

import (
	"fmt"
)

const SUCCESS = 0

type Result struct {
	Code    int         `json:"errorCode"`
	Message string      `json:"errorMsg"`
	Data    interface{} `json:"response"`
}

func (r *Result) Success() bool {
	return r.Code == SUCCESS
}

/**
 * 返回一个成功的结果
 * @param data: 结果数据
 * @return: 一个成功的返回信息
 */
func NewSuccessResult(data interface{}, msg ...interface{}) *Result {
	message := "Done"
	if len(msg) > 0 {
		message = fmt.Sprint(msg...)
	}
	return &Result{
		Code:    SUCCESS,
		Data:    data,
		Message: message,
	}
}

/**
 * 返回一个失败的结果
 * @param errorCode: 错误码
 * @param msg: 错误描述
 * @return: 一个失败的结构体
 */
func NewFailResult(errorCode int, msg string) *Result {
	return &Result{
		Code:    errorCode,
		Data:    nil,
		Message: msg,
	}
}

func NewFailResultf(errorCode int, format string, args ...interface{}) *Result {
	return &Result{
		Code:    errorCode,
		Data:    nil,
		Message: fmt.Sprintf(format, args...),
	}
}

type TaskResult struct {
	TaskId    int64
	Status    string
	Action    string
	ErrorCode int
	Progress  float64
}

type OperateResult struct {
	Success int
	Fail    int
}
