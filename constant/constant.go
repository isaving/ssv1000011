//Version: v0.0.1
package constant

const (
	AC000002            = "AC000002"
	DLS_TYPE_CMM        = "CMM"
	DLS_TYPE_ELEMENT_ID = "common"
)

//define error code
const (
	ERRCODE01 = "SV99000020"
)
